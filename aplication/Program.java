package aplication;

import java.util.Locale;
import java.util.Scanner;

import entities.Employee;

public class Program {

	public static void main(String[] args) {
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Name: ");
		String name = sc.nextLine();
		
		System.out.print("Gross Salary: ");
		double grossSalary = sc.nextDouble();
		
		System.out.print("Tax: ");
		double tax = sc.nextDouble();
		System.out.println();
		
		Employee employee = new Employee(name, grossSalary, tax);
		
		System.out.printf("Employee: " + employee.name + ", $%.2f", (employee.NetSalary()));
		System.out.println();
		System.out.println();
		
		System.out.print("Which percentage to increase salary? ");
		double percentage = sc.nextDouble();
		employee.IncreaseSalary(percentage);
		System.out.println();
		
		System.out.printf("Update data: " + employee.name + ", $%.2f", (employee.NetSalary()));
		
		
		sc.close();
	}

}
