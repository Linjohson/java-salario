package entities;

public class Employee {
	
	public String name;
	public double grossSalary;
	public double tax;
	
	//Construtores
	public Employee() {
		
	}
	
	public Employee(String name, double grossSalary, double tax) {
		this.name = name;
		this.grossSalary = grossSalary;
		this.tax = tax;
	}
	
	public Employee(String name, double grossSalary) {
		this.name = name;
		this.grossSalary = grossSalary;
	}
	
	//M�todos
	public double NetSalary() {
		return grossSalary - tax;
	}
	
	public void IncreaseSalary(double percentage) {
		grossSalary += grossSalary * percentage / 100; 
	}
	
//	public String toString(double percentage) {
//		return  "Employee: " + name + "%n" +
//				"Which percentage to increase salary? " + percentage +
//				"Update data: " + name + ", " + String.format("%.2f", NetSalary());
//	}
}
